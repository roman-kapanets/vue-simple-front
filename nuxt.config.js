module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: "vue-simple-front",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: "Vue.js simple description"
      }
    ],
    link: [
      {
        rel: "icon",
        type: "image/x-icon",
        href: "/favicon.ico"
      }
    ]
  },
  /*
  ** Server configuration
  */
  server: {
    port: 3001, // default: 3000
    host: "127.0.0.1" // default: localhost
  },
  /**
   ** Modules
   */
  modules: ["@nuxtjs/axios"],
  /*
  ** Plugins
  */
  plugins: ["~/plugins/axios"],
  /**
   ** Environment variables
   */
  env: {
    baseUrl: process.env.BASE_URL || "http://localhost:3001"
  },
  /*
  ** Axios settings
  */
  axios: {
    baseURL: "https://vue-simple-api.onnet.work/"
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: "#3B8070" },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          loader: "eslint-loader",
          exclude: /(node_modules)/
        })
      }
    }
  }
}
